#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_HM2014811.mk

COMMON_LUNCH_CHOICES := \
    omni_HM2014811-user \
    omni_HM2014811-userdebug \
    omni_HM2014811-eng
